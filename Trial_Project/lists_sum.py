from itertools import chain
from functools import reduce

def array_sum(*iterable):
    for i in iterable:
        if isinstance(i, str) or isinstance(i, int):
            raise ValueError("Iterable is required")
    whole_list = list(chain(*iterable))
    if len(whole_list) == 0:
        raise ValueError("Empty list")
    else:
        return reduce(lambda x, y: x + y, whole_list)
