import lists_sum
import unittest

class TestSumFunction(unittest.TestCase):

    def test_empty_lists(self):
        with self.assertRaisesRegex(ValueError, "Empty list"):
            lists_sum.array_sum([], [], [])

    def test_sum(self):
        self.assertEqual(lists_sum.array_sum([1, 2], [3, 4, 5, 6], [7, 8, 9], [10]), 55)

    def test_one_list(self):
        self.assertEqual(lists_sum.array_sum([1, 3, 4]), 8)

    def test_zero(self):
        self.assertEqual(lists_sum.array_sum([0, 0, 0, 0], [0, 0]), 0)

    def test_tuple(self):
        self.assertEqual(lists_sum.array_sum((1, 2, 3, 4), (2, 6)), 18)

    def test_str(self):
        with self.assertRaisesRegex(ValueError, "Iterable is required"):
            lists_sum.array_sum("1, 3, 4, 5, 6, 7, 8")

    def test_int(self):
        with self.assertRaisesRegex(ValueError, "Iterable is required"):
            lists_sum.array_sum(1, 2, 3, 4, 5, 67, 8)



if __name__ == '__main__':
    unittest.main()