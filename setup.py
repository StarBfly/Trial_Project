from setuptools import setup

setup(name='trial_project',
      version='0.1',
      description='sum_function',
      url='https://gitlab.com/StarBfly/Trial_Project',
      author='Margaret Butterfly',
      license='MIT',
      entry_points={
            'console.scripts':[
                  'sum = Trial_Project.lists_sum:array_sum',
            ]
      },
      zip_safe=False,
)
